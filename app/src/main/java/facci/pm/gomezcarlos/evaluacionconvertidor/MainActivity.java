package facci.pm.gomezcarlos.evaluacionconvertidor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button button;
    EditText EditTextIngreso;
    TextView textViewresultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.buttonConvertidor);
        EditTextIngreso = (EditText) findViewById(R.id.editTextIngreso);
        textViewresultado = (TextView) findViewById(R.id.textViewresultado);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Pantalla2.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato", textViewresultado.getText().toString());
                float valor = Float.parseFloat(EditTextIngreso.getText().toString());
                float resultadoC = (float) ((valor * 0.000453592));
                textViewresultado.setText(String.format("%.9f", resultadoC, "toneladas"));
                bundle.putString("dato", textViewresultado.getText().toString());

                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

    }
}
