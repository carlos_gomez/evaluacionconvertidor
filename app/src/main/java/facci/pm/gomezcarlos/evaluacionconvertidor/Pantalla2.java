package facci.pm.gomezcarlos.evaluacionconvertidor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class Pantalla2 extends AppCompatActivity {

    TextView textViewresultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla2);

        textViewresultado = (TextView)findViewById(R.id.textViewresultado);

        Bundle bundle = this.getIntent().getExtras();
        textViewresultado.setText(bundle.getString("dato"));

        Log.e("Conversión", "Finalizada");
    }

}

